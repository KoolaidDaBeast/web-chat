//const API_URL = 'http://localhost:5000/api';
const API_URL = 'http://basic-web-chat-api.herokuapp.com/api';

function enableTooltips() {
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
}

function showPage() {
    $("#overlay").hide();
    $("#main").show();
}

async function getUserInput(question) {
    const result = await Swal.fire({
        title: question,
        input: 'text',
        showCancelButton: true,
        inputValidator: (value) => {
            if (!value) {
                return 'You need to write something!'
            }
        }
    });

    return result.value;
}

function redirectAlert(title, body, type, link) {
    Swal.fire({
        title: title,
        text: body,
        icon: type
    }).then((result) => {
        window.location.href = link;
    });
}

function confirmAlert(title, body, type, confirmText, func) {
    Swal.fire({
        title: title,
        text: body,
        icon: type,
        showCancelButton: true,
        confirmButtonText: confirmText
    }).then((result) => {
        if (result.value) {
            func();
        }
    });
}

//Select Profile File Event
$("#profile-image").change((e) => {
    const file = e.target;

    if (file.files[0] == undefined) { return; }

    $(".custom-file-label").text(file.files[0].name);
    $("#register-button").attr('disabled', '');

    //Convert file into base64 string
    var reader = new FileReader();
    reader.readAsBinaryString(file.files[0]);

    reader.onload = function() {
        localStorage.setItem('fileDump', btoa(reader.result));
        $("#register-button").removeAttr('disabled');
    };
    reader.onerror = function() {
        console.error('ERROR: Failed to convert image to base64.');
    };
});

$("#register-button").click(() => {
    const displayName = $('#username').val();
    const email = $('#email').val();
    const password = $('#password').val();
    const profileImage = localStorage.getItem('fileDump');

    $.post(`${API_URL}/account/register`, { displayName, email, password, profileImage }).then((response) => {
        if (response.success == false) {
            Swal.fire('Error', response.message, 'warning');
            return;
        }

        redirectAlert('Good Job', response.message, 'success', '/chats/0');
    }).fail(((xhr, textStatus, errorThrown) => {
        Swal.fire('Error 404', 'Failed to contact servers. Please try again later!', 'error');
    }));
});

$("#login-button").click(() => {
    const email = $('#email').val();
    const password = $('#password').val();

    $.post(`${API_URL}/account/auth`, { username: email, password, key: localStorage.getItem('apiKey') }).then((response) => {
        if (response.success == false) {
            Swal.fire('Error', response.message, 'warning');
            return;
        }

        localStorage.setItem('apiKey', response.key);
        localStorage.setItem('userId', response.userId);
        localStorage.setItem('displayName', response.displayName);

        redirectAlert('Good Job', response.message, 'success', '/chats/0');
    }).fail(((xhr, textStatus, errorThrown) => {
        Swal.fire('Error 404', 'Failed to contact servers. Please try again later!', 'error');
    }));
});

$("#create-server-button").click(() => {
    const name = $('#name').val();
    const serverIcon = localStorage.getItem('fileDump');

    $.post(`${API_URL}/server/manage`, { name, key: localStorage.getItem('apiKey'), icon: serverIcon }).then((response) => {
        if (response.success == false) {
            Swal.fire('Error', response.message, 'warning');
            return;
        }

        redirectAlert('Good Job', response.message, 'success', `/chats/${response.id}`);
    }).fail(((xhr, textStatus, errorThrown) => {
        Swal.fire('Error 404', 'Failed to contact servers. Please try again later!', 'error');
    }));
});

$("#join-server-button").click(async() => {
    const serverId = await getUserInput("Server ID?");
    $.ajax({
        url: `${API_URL}/server/manage`,
        method: 'PUT',
        data: { id: serverId, key: localStorage.getItem('apiKey') },
        success: function(response) {
            if (response.success) {
                window.location.reload();
                return;
            }

            Swal.fire('Uh Oh!', response.message, 'warning');
        },
        error: function(param) {
            Swal.fire('Error 404', 'Failed to contact servers. Please try again later!', 'error');
        }
    });
});

$("#leave-server-button").click(() => {
    const serverId = window.location.href.split('/')[4];

    confirmAlert('Are you sure?', 'Are you sure you want to leave this server?', 'error', 'Leave Server', function() {
        $.ajax({
            url: `${API_URL}/server/manage`,
            method: 'DELETE',
            data: { id: serverId, key: localStorage.getItem('apiKey') },
            success: function(response) {
                window.location.href = "/chats/0";
            },
            error: function(param) {
                Swal.fire('Error 404', 'Failed to contact servers. Please try again later!', 'error');
            }
        });

    });
});